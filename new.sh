# Pipes
# Download the contents of "Harry Potter and the Goblet of fire" using the command line

wget "https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt"

# Print the first three lines in the book

head -3 "Harry Potter and the Goblet of Fire.txt"

# Print the last 10 lines in the book

tail -10 "Harry Potter and the Goblet of Fire.txt"

# How many times do the following words occur in the book?
#Harry
grep -o -i  Harry "Harry Potter and the Goblet of Fire.txt" | wc -l

#Ron
grep -o -i  Ron "Harry Potter and the Goblet of Fire.txt" | wc -l

#Hermione
grep -o -i  Hermione "Harry Potter and the Goblet of Fire.txt" | wc -l

#Dumbledore
grep -o -i  Dumbledore "Harry Potter and the Goblet of Fire.txt" | wc -l

# Print lines from 100 through 200 in the book
head -100 "Harry Potter and the Goblet of Fire.txt"| tail -200

# How many Unique words are present in the book
tr ' ' '\n' < Harry\ Potter\ and\ the\ Goblet\ of\ Fire.txt | sort | uniq -c | wc -



# Process,Ports
# List your browser's process ids(pid)and parent process ids(ppid)
ps -ef

# Stop the browser application from the command line
killall firefox

# List the top 3 processes by CPU usage
ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%cpu |head -n 4

# List the top 3 processes by memory usage
ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem |head -n 4

# Start a Python HTTP server on port 8000
python3 -m http.server 8000

# Open another tab. Stop the process you started in previous step
gnome-terminal
exit

# Start a Python HTTP server on port 90
sudo python3 -m http.server 90

# Display all active connections and the corresponding TCP / UDP ports
netstat -a

# Find the pid of the process that is listening on port 5432
lsof -i :5432



# managing software
# Install htop, vim and nginx
sudo apt install htop
sudo apt install vim
sudo apt install nginx

# Uninstall nginix
sudo apt remove nginx


# Misc
# What's your local IP address
hostname -I

# Find the IP address of google.com
nslookup
>google.com

# How to check if internet is working using CLI
ping -c 3 google.com

# Where is the node Command located? What about code?
whereis node
whereis code

